// FIXAndDataStructuresChallenge.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include "FIX42subset.h"

#ifdef _C17_FROM_CHARS
#include <charconv>
#endif

#include <string>
#include <list>
#include <vector>
#include <unordered_map>
#include <set>

#include <fstream>
#include <iostream>

using namespace std;

static const char msg_logon = 'A';
static const char msg_new_order = 'D';
static const char msg_execution_report = '8';

// Pertinent Fields for this example
enum {
	field_Account = 1,
	field_BodyLength = 9,
	field_CheckSum = 10,
	field_Price = 44,
	field_Symbol = 55,
	field_NoContraBrokers = 382,

	// contra_brokers_repeating_group
	field_ContraBroker = 375,
	field_ContraTrader = 337,
	field_ContraTradeQty = 437,
	field_ContraTradeTime = 438
};


struct contra_brokers_repeating_group
{
//	int _tag;
	string _field;
	char _required;	/// = 'N';
};

std::unordered_map<int, contra_brokers_repeating_group> cbrg_map
{
	{375, {"ContraBroker, 'C'"}},
	{337, {"ContraTrader, 'N'"}},
	{437, {"ContraTradeQty", 'N'}},
	{438, {"ContraTradeTime", 'N'}}
};

//#define estimated_max_tags_in_msg 5000
#define soh '|'
#define eq  '='


struct PriceMinMax
{
	PriceMinMax(double price)
	{
		_min = _max = price;
	}

	void UpdateMinMax(double price)
	{
		_min = min(_min, price);
		_max = max(_max, price);
	}
	double getMin() {
		return _min;
	}
	double getMax() {
		return _max;
	}
	double _min, _max;
};

class CFIXChallenge 
{
public:
	CFIXChallenge() /*: tags(estimated_max_tags_in_msg)*/ {}

public:
	bool ReplayFIXMessagesFromFile(string& filename);
	void PrintAccountMinMaxPrices();

private:
	// character by character parsing
	inline void VERIFY(const char* p, char c);
	bool ParseCharByChar(string& line);

	// using delimiter parsing
	bool ParseAFIXMessage(string& line);
	bool ProcessExecutionReportContraRepeatingGroup(string& s, size_t& start, size_t& end, int count);


	unordered_map<size_t, string> tag_value_pairs;
//	vector<string> tags;
	unordered_map<string, PriceMinMax>	account_minmax_map;
//	size_t errors = 0;

};


bool CFIXChallenge::ReplayFIXMessagesFromFile(string& filename)
{
	ifstream ifs(filename.c_str());
	if (!ifs)
	{
		cerr << "Could not open file\n";
		return false;
	}
	int count = 0;
	std::string line;
	while (getline(ifs, line))
	{
		//LOG_INFO << "Replaying #" << ++count << ", " << line << std::endl;
		//FIX8::CMETC::TradeCaptureReport* msg = new FIX8::CMETC::TradeCaptureReport;
		//msg->decode(line);

		ParseAFIXMessage(line);

		//FIX8::Sleep(200);
	}
	ifs.close();
	return true;
}

bool CFIXChallenge::ProcessExecutionReportContraRepeatingGroup(string& s, size_t& start, size_t& end, int count)
{
	if (end == std::string::npos)
		return false;

	// tag, value
	unordered_map<size_t, string> tag_value_pairs;

	start = end + 1;
	end = s.find_first_of(soh, start);

	size_t i = 0;
	while (i < count && end < string::npos)
	{
		// Find delimiter location for tag = value
		size_t loc_eq = s.find(eq, start);
		if (loc_eq == string::npos)
		{
			cerr << "Error finding '=' delim.\n";
			return false;
		}

		size_t tag = stoi(s.substr(start, loc_eq - start));
		loc_eq++;

		// Check for valid fields in repeating group
		switch (tag)
		{
		case field_ContraBroker:
			if (tag_value_pairs.find(tag) != tag_value_pairs.end())
			{
				// tag exists - process map fields if haven't already
				tag_value_pairs.clear();
				i++;
			}		
			tag_value_pairs.emplace(tag, s.substr(loc_eq, end - loc_eq));
			cout << "  repeating group " << i << ": tag " << tag << "\n";
			break;

		case field_ContraTrader:
		case field_ContraTradeQty:
		case field_ContraTradeTime:
			// check for duplicate field
			if (tag_value_pairs.find(tag) != tag_value_pairs.end())
			{
				// tag exists - throw a duplicate field error
				cerr << "Duplicate field error in repeating group. Skipping\n";
			}
			else
				tag_value_pairs.emplace(tag, s.substr(loc_eq, end - loc_eq));
				cout << "  repeating group " << i << ": tag " << tag << "\n";
			break;

		default:
			// Field is not part of the repeating group - this should trigger the end of group
			if (++i >= count)
				;// process map fields if haven't already
			else
			{
				cerr << "Repeating Group records " << count << " do not match the expected count\n";
				i = count;	// force end of loop
			}
			break;
		};

		if (end == std::string::npos)
			break;

		if (i < count)
		{
			start = end + 1;
			end = s.find_first_of(soh, start);
		}
	}
	return true;
}

bool CFIXChallenge::ParseAFIXMessage(string& s)
{
	// Process the message in line
	size_t start = 0;
	size_t end = s.find_first_of(soh);
	size_t body_length_start = 0;
	size_t nest_level = 0;

	string account;
	double price = NULL;

	// or use a vector with pre-allocated memory
	// tag_value_vector.clear();

	tag_value_pairs.clear();
	while (end < string::npos)
	{
		size_t loc_eq = s.find(eq, start);
		if (loc_eq == string::npos)
		{
			cerr << "Error finding '=' delim.\n";
			return false;
		}
		

#ifdef _C17_FROM_CHARS
		// faster int parsing - C++17
		const auto res = std::from_chars(...);
#else
		size_t tag = stoi(s.substr(start, loc_eq - start));
#endif



		// Get the Value
		loc_eq++;
		tag_value_pairs.emplace(tag, s.substr(loc_eq, end - loc_eq));

		auto ptr = tag_value_pairs.emplace(tag, s.substr(start, loc_eq - start));
		if (ptr.second)
			cerr << "Duplicate tag " << tag << " in msg '" << s << "'\n";

#ifdef _VERBOSE
		cout << tag << " " << ptr.first->second << "\n";
#endif

		switch (tag)
		{
		case field_BodyLength:
			body_length_start = end;	// possibly used for body length check
			break;

		case field_CheckSum:
			break;

		case field_Account:	
			account = ptr.first->second;
			break;

		case field_Symbol:
			break;

		case field_Price:
			price = std::stod(ptr.first->second);
			break;

		default:
			break;

		// each msg_type could be in separate classes
		case field_NoContraBrokers:		// Execution Report
			nest_level++;	
			size_t brokers_expected = stoi(ptr.first->second);
			if (ProcessExecutionReportContraRepeatingGroup(s, start, end, brokers_expected))
				continue;
			break;

		};


		if (end == std::string::npos)
			break;

		start = end + 1;
		end = s.find_first_of(soh, start);
	}

	cout << "\n";


	// Check/set min/max price for account
	if (!account.empty() && price != NULL)
	{
		auto it = account_minmax_map.find(account);
		if (it == account_minmax_map.end())
			account_minmax_map.emplace(account, PriceMinMax(price));
		else
			it->second.UpdateMinMax(price);
	}
	return true;
}

void CFIXChallenge::PrintAccountMinMaxPrices()
{
	cout << "\nMix/Max Prices by Account\n";
	for (auto it = account_minmax_map.begin(); it != account_minmax_map.end(); it++)
	{
		cout << it->first << "  min: " << it->second.getMin() << "  max: " << it->second.getMax() << "\n";
	}
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu
int main()
{
	std::cout << "Hello World!\n";
	CFIXChallenge fix;

	string sample_file = "Fix.Sample.Txt";
	fix.ReplayFIXMessagesFromFile(sample_file);

	// Print Min/Max prices per account
	fix.PrintAccountMinMaxPrices();

}





#ifdef _CHARBYCHAR


inline void CFIXChallenge::VERIFY(const char* p, char c)
{
	if (*p++ != (c)) 
	{ 
		cerr << "Unexpected character " << c << "\n";
		errors++;
		return; 
	}
}

// Better Performance Option
bool CFIXChallenge::ParseCharByChar(string& line)
{
	const char* p = line.c_str();

	errors = 0;

	// All messages in this example should start with 8=FIX.4.2
	VERIFY(p, '8');
	VERIFY(p, '=');
	VERIFY(p, 'F');
	VERIFY(p, 'I');
	VERIFY(p, 'X');
	VERIFY(p, '.');
	VERIFY(p, '4');
	VERIFY(p, '.');
	VERIFY(p, '2');
	VERIFY(p, soh);

	// Body Length
	VERIFY(p, '9');
	VERIFY(p, '=');
	while (*p != soh)
	{

	}
	return true;
}


void SplitStringToVector(vector<std::string>& vec, const string& s, const char delimiter)
{
	vec.clear();

	size_t start = 0;
	size_t end = s.find_first_of(delimiter);


	while (end <= string::npos)
	{
		vec.emplace_back(s.substr(start, end - start));

		if (end == string::npos)
			break;

		start = end + 1;
		end = s.find_first_of(delimiter, start);
	}

}
#endif